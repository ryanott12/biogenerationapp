package edu.svsu.biogeneration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

public class BioGenerationActivity extends Activity {
    String degree;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_generation);
    }

    public void onRadioButtonClicked(View view){
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        //Fill variable for degree type
        switch(view.getId()) {
            case R.id.bsRadio:
                if (checked)
                    degree = "B.S.";
                    break;
            case R.id.basRadio:
                if (checked)
                    degree   = "BAS";
                    break;
            case R.id.baRadio:
                if (checked)
                    degree   = "B.A.";
                break;
            case R.id.bfaRadio:
                if (checked)
                    degree   = "BFA";
                break;
            case R.id.msRadio:
                if (checked)
                    degree   = "M.S.";
                break;
            case R.id.maRadio:
                if (checked)
                    degree   = "M.A.";
                break;
            case R.id.mfaRadio:
                if (checked)
                    degree   = "MFA";
                break;
            case R.id.mbaRadio:
                if (checked)
                    degree   = "MBA";
                break;
            case R.id.phdRadio:
                if (checked)
                    degree   = "Ph.D.";
                break;
            case R.id.mdRadio:
                if (checked)
                    degree   = "M.D.";
                break;
            case R.id.ddsRadio:
                if (checked)
                    degree   = "DDS";
                break;
            case R.id.jdRadio:
                if (checked)
                    degree   = "J.D.";
                break;
        }
    }

    public void onSendMessage(View view) {
        //Fill variable for first name
        EditText messageView = (EditText)findViewById(R.id.firstnameEditText);
        String firstName = messageView.getText().toString();

        //Fill variable for last name
        messageView = (EditText)findViewById(R.id.lastnameEditText);
        String lastName = messageView.getText().toString();

        //Fill variable for school
        messageView = (EditText)findViewById(R.id.schoolEditText);
        String school = messageView.getText().toString();

        //Fill variable for year of graduation
        messageView = (EditText)findViewById(R.id.yearEditText);
        String year = messageView.getText().toString();

        //Fill variable for activities
        messageView = (EditText)findViewById(R.id.activitiesEditText);
        String activities = messageView.getText().toString();

        //Fill variable for major selected
        Spinner spinner = (Spinner)findViewById(R.id.majorSpinner);
        String major = String.valueOf(spinner.getSelectedItem());

        String messageText = (firstName + " " + lastName + " graduated in " + year + " with a " +
                degree + " with a concentration in " + major + " from " + school +
                ". Their favorite activities are " + activities + ".");

        Intent intent = new Intent(this, BioCreationActivity.class);
        intent.putExtra(BioCreationActivity.EXTRA_MESSAGE, messageText);
        startActivity(intent);
    }
}
